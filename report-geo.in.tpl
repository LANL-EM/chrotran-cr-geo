template !
# Id: report-geo, Wed 31 May 2017 10:32:53 AM MDT pandeys #
# Created by Sachin Pandey, LANL
# Description: Full dithionite reactions
#   - porewater velocity is 0.1 m/d (~ R-42)
#------------------------------------------------------------------------------

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE RICHARDS
    /
    SUBSURFACE_TRANSPORT transport
      GLOBAL_IMPLICIT
      NUMERICAL_JACOBIAN
    /
  /
END

SUBSURFACE

#=========================== chemistry ========================================
CHEMISTRY
  PRIMARY_SPECIES
    H+
    O2(aq)
    CrO4--
    S2O4--
    S2O3--
    SO3--
    SO4--
    Fe+++
    Cr+++
    HCO3-
    Ca++
  /
  SECONDARY_SPECIES
    OH-
    CO3--
    CO2(aq)
  /
  IMMOBILE_SPECIES
    slow_Fe++
    fast_Fe++
  /
  REACTION_SANDBOX
    DITHIONITE_PARAMETERS
      K_S2O4_DISP       !K_S2O4_DISP! # log10 = -4.5
      K_S2O4_O2         !K_S2O4_O2!
      K_S2O4_FE3        !K_S2O4_FE3!
      K_FE2_O2_FAST     !K_FE2_O2_FAST!
      K_FE2_O2_SLOW     0.0d0
      K_FE2_CR6_FAST    !K_FE2_CR6_FAST!
      K_FE2_CR6_SLOW    0.0d0
      ALPHA             1.0d0
      ROCK_DENSITY      1200.d0   # kg/m^3_bulk
      SSA_FEOH3         175.d0    # m^2/g
      EPS               1.d-30
    /
  /
  MINERALS
    Fe(OH)3(s)
    Cr(OH)3(s)
    Calcite
  /
  MINERAL_KINETICS
    Fe(OH)3(s)
      RATE_CONSTANT 1.d-5 mol/m^2-sec
    /
    Cr(OH)3(s)
      RATE_CONSTANT 1.d-5 mol/m^2-sec
    /
    Calcite
      RATE_CONSTANT 1.d-5 mol/m^2-sec
    /
  /
  DATABASE ../../chromium.dat
  ACTIVITY_COEFFICIENTS TIMESTEP
  LOG_FORMULATION
  OUTPUT
    ALL
    FREE_ION
    TOTAL
    PH
  /
END

#=========================== water options =+==================================
EOS WATER
  DENSITY CONSTANT 999.10d0 kg/m^3 # 15 deg C
  VISCOSITY CONSTANT 0.0010016d0 # Pa - s (i.e. dynamic viscocity)
END

#=========================== solver options ===================================
TIMESTEPPER FLOW
  TS_ACCELERATION 8
END

NEWTON_SOLVER TRANSPORT
  STOL 1.d-30
  ITOL 1.d-8
  RTOL 1.d-8
  MAXIT 25
END

#=========================== discretization ===================================
GRID
  TYPE structured
  NXYZ 600 150 1
  BOUNDS
    0.0 0.0 0.0
    600.0 150.0 30.0
  /
END

#============================== dataset =======================================
DATASET Permeability
  FILENAME ../../domains/rand_600x150_corr10_sig1_20mpd.h5
END

#=========================== fluid properties =================================
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END

#=========================== material properties ==============================
MATERIAL_PROPERTY soil1
  ID 1
  POROSITY 0.15d0
  TORTUOSITY 1.d0
  SATURATION_FUNCTION cc1
  ROCK_DENSITY 1200.d0 # kg/m^3_bulk
  PERMEABILITY
    DATASET Permeability
  /
#  LONGITUDINAL_DISPERSIVITY 5.d0
END

#=========================== characteristic curves ============================
CHARACTERISTIC_CURVES cc1
  SATURATION_FUNCTION VAN_GENUCHTEN
    ALPHA  1.d-4
    M 0.5d0
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_LIQ
    M 0.5d0
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
END

#=========================== output options ===================================
OUTPUT
#  FORMAT TECPLOT BLOCK
#  FORMAT HDF5
  VELOCITY_AT_CENTER
  VARIABLES
    LIQUID_SATURATION
    LIQUID_PRESSURE
    LIQUID_HEAD 
    PERMEABILITY
  END
#  PERIODIC TIME 1.d0 d
  PERIODIC_OBSERVATION TIME 1.d0 d
  PRINT_COLUMN_IDS
#  MASS_BALANCE_FILE
#    PERIODIC TIME  1.d0 d
#  /
END

#=========================== times ============================================
TIME
  FINAL_TIME 365.d0 d
  INITIAL_TIMESTEP_SIZE 1.d-12 y 
  MAXIMUM_TIMESTEP_SIZE 1.d0 d at 0.0 d
  MAXIMUM_TIMESTEP_SIZE 1.d-4 d at 10.0 d
  MAXIMUM_TIMESTEP_SIZE 1.d-1 d at 10.0001 d
  MAXIMUM_TIMESTEP_SIZE 1.d0 d at 30.0 d
END

#=========================== regions ==========================================
REGION all
  COORDINATES
     0.0 0.0 0.0
     600.0 150.0 30.0
  /
END

REGION west
  FACE WEST
  COORDINATES
     0.0 0.0 0.0
     0.0 150.0 30.0
  /
END

REGION east
  FACE EAST
  COORDINATES
     600.0 0.0 0.0
     600.0 150.0 30.0
  /
END

REGION south
  FACE SOUTH
  COORDINATES
     0.0 0.0 0.0
     600.0 0.0 30.0
  /
END

REGION north
  FACE NORTH
  COORDINATES
     0.0 150.0 0.0
     600.0 150.0 30.0
  /
END

REGION well_injection
  COORDINATES
    40.50 75.50 0.0
    40.50 75.50 30.0
  /
END

REGION obs1
  COORDINATES
  50.50 75.5 0.0
  50.50 75.5 30.0
  /
END

REGION obs2
  COORDINATES
  60.50 75.5 0.0
  60.50 75.5 30.0
  /
END

REGION obs3
  COORDINATES
  70.50 75.5 0.0
  70.50 75.5 30.0
  /
END

REGION obs4
  COORDINATES
  80.50 75.5 0.0
  80.50 75.5 30.0
  /
END

#================== observation points ========================
OBSERVATION obs1
  REGION obs1
/

OBSERVATION obs2
  REGION obs2
/

OBSERVATION obs3
  REGION obs3
/

OBSERVATION obs4
  REGION obs4
/

OBSERVATION well_injection
  REGION well_injection
/

#=========================== flow conditions ==================================
FLOW_CONDITION initial
  TYPE
    PRESSURE hydrostatic
  /
  DATUM 0.d0 0.d0 0.d0
  PRESSURE 9.78924583e+06
END

FLOW_CONDITION no_flow
  TYPE
    FLUX neumann
  /
  FLUX 0.d0
END

FLOW_CONDITION west
  TYPE
    PRESSURE hydrostatic
  /
  DATUM 0.d0 0.d0 0.d0
  PRESSURE 9.78924583e+06
END

FLOW_CONDITION east
  TYPE
    PRESSURE hydrostatic
  /
  DATUM 0.d0 0.d0 0.d0
  PRESSURE 9.78484067e+06
END

FLOW_CONDITION well
  TYPE
    RATE mass_rate
  /
  RATE list
    TIME_UNITS d
    DATA_UNITS kg/s
    0.0 0.0
    10.0 0.6298
    30.0 0.0
  /
END

#=========================== transport conditions =============================
TRANSPORT_CONDITION initial
  TYPE dirichlet
  CONSTRAINT_LIST
    0.d0 initial
  /
END

TRANSPORT_CONDITION inlet
  TYPE dirichlet_zero_gradient
  CONSTRAINT_LIST
    0.d0 inlet
  /
END

TRANSPORT_CONDITION outlet
  TYPE dirichlet_zero_gradient
  CONSTRAINT_LIST
    0.d0 inlet
  /
END

TRANSPORT_CONDITION injectant
  TYPE dirichlet
  CONSTRAINT_LIST
    0.d0 injectant
  /
END

#=========================== constraints ======================================
CONSTRAINT initial
  CONCENTRATIONS
    H+ -1.1723e-04 T
    O2(aq) 2.8400e-04 T
    CrO4-- 1.9200e-05 T
    S2O4-- 1.0000e-20 T
    S2O3-- 1.0000e-20 T
    SO3-- 1.0000e-20 T
    SO4-- 1.0000e-20 T
    Fe+++ 1.0000e-20 T
    Cr+++ 1.0000e-20 T
    HCO3- 1.1723e-04 T
    Ca++ 1.1723e-04 T
  /
  MINERALS
    Fe(OH)3(s) !IFEOH3! 1.d3 # 1% mass percent
    Cr(OH)3(s) 1.d-20 1.d3
    Calcite 2.2099e-02 1.d3
  /
  IMMOBILE
    slow_Fe++ 1.0d-20
    fast_Fe++ 1.0d-20
  /
END

CONSTRAINT inlet
  CONCENTRATIONS
    H+ -1.1723e-04 T
    O2(aq) 2.8400e-04 T
    CrO4-- 1.9200e-05 T
    S2O4-- 1.0000e-20 T
    S2O3-- 1.0000e-20 T
    SO3-- 1.0000e-20 T
    SO4-- 1.0000e-20 T
    Fe+++ 1.0000e-20 T
    Cr+++ 1.0000e-20 T
    HCO3- 1.1723e-04 T
    Ca++ 1.1723e-04 T
  /
END

CONSTRAINT injectant
  CONCENTRATIONS
    H+     -0.4 T
    O2(aq)  2.84d-4 T
    CrO4--  1.d-20 T
    S2O4--  1.d-2 T
    S2O3--  1.d-20 T
    SO3--   1.d-20 T
    SO4--   1.d-20 T
    Fe+++   1.d-20 T
    Cr+++   1.d-20 T
    HCO3-   0.4d0 T
    Ca++    1.d-20 T
  /
END

#=========================== condition couplers ===============================
# initial condition
INITIAL_CONDITION
  TRANSPORT_CONDITION initial
  FLOW_CONDITION initial
  REGION all
END

BOUNDARY_CONDITION west
  TRANSPORT_CONDITION inlet
  FLOW_CONDITION west 
  REGION west 
END

BOUNDARY_CONDITION east  
  TRANSPORT_CONDITION outlet
  FLOW_CONDITION east 
  REGION east
END

SOURCE_SINK
  TRANSPORT_CONDITION injectant
  FLOW_CONDITION well
  REGION well_injection
END

#=========================== stratigraphy couplers ============================
STRATA
  REGION all 
  MATERIAL soil1
END

END_SUBSURFACE
