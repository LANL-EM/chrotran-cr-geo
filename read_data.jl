using HDF5
import Pflotran

function readdata(d)
    
    function runforabit(command, waittime=0, pollinterval=1)
        # kills terminal command if time limit is exceeded
        # note, all times are in seconds
        starttime = now()
        cmdproc = spawn(command)
        if waittime > 0
            timedwait(() -> process_exited(cmdproc), waittime)
            if process_running(cmdproc)
                warn("Simulation failed on worker ??")
                kill(cmdproc)
                return false
            end
        else
            wait(cmdproc)
        end
    end
    
    function parseh5!(casetag::AbstractString, results)
        # User Data
        np = 16
        maxruntime = 2.0 * 60.0 * 60.0 # seconds from hours

        pfpath = "/lclscratch/sach/Programs/chrotran/release-2.0/src/pflotran"
        masstag = casetag * "-obs-6.tec"
        otag = casetag * ".out"
        crtag = "obs_t"

        # remove old files
        run(`rm -f $masstag`)
        run(`rm -f $otag`)

        directive = "mpirun -np $(np) " * pfpath * "/chrotran -pflotranin " * casetag * ".in >barf.txt"
        asdf = `bash -c "$directive"`
        runforabit(asdf, maxruntime)

        # METHOD 2: USE THE MASS BALANCE FILE
        myvar = ["Total CrO4-- [M] obs1"] # variable name
        mydata = Pflotran.readObsDataset(masstag,myvar)
        obstimes = mydata[:,1]
        targets = mydata[:,2]
        for i in 1:length(obstimes)
            results[crtag * "$(i)"] = targets[i]
        end

        # Check to make sure you have all of the observations
        obskeys = Mads.getobskeys(d)
        if length(results) != length(obskeys)
            error("Number of simulated results does not match number of obskeys!!")
        end
        
        return results
    end

    results = DataStructures.OrderedDict{AbstractString,Any}()
    parseh5!("report-geo", results)
    return results
end
